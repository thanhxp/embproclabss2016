-- ===================================================================
-- (C)opyright 2001, 2002
-- 
-- Lehrstuhl Entwurf Mikroelektronischer Systeme
-- Prof. Wehn
-- Universitaet Kaiserslautern
-- 
-- ===================================================================
-- 
-- Autoren:  Frank Gilbert
--           Christian Neeb
--           Timo Vogt
-- 
-- ===================================================================
-- 
-- Projekt: Mikroelektronisches Praktikum
--          SS 2002
-- 
-- ===================================================================
-- 
-- Modul:
-- DLX Pipeline: Instruction Fetch Stufe.
-- 
-- ===================================================================
--
-- $Author: gilbert $
-- $Date: 2002/06/18 09:27:04 $
-- $Revision: 2.0 $
-- 
-- ===================================================================

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.dlx_global.all;

-- ===================================================================
entity dlx_pipe_if is
	port(
		clk       : in  std_logic;      -- System Clock
		rst       : in  std_logic;      -- System Reset, active HIGH, synchronous
		stall     : in  std_logic;      -- Data dependency detected, need to stall this stage, from CTRL
		dc_wait   : in  std_logic;      -- Wait for memory to finish read operation --> stalled pipeline, from CTRL
		id_npc    : in  dlx_word_us;    -- Next PC counter in case of branch and jump, from ID stage 
		id_cond   : in  std_logic;      -- Condition value from ID stage to take PC+4 or id_npc to be fetched
		ic_data   : in  dlx_word;       -- Instruction cache data
		ic_addr   : out dlx_address;    -- Instruction cache address
		if_id_ir  : out dlx_word;       -- Pipelined Intruction register, to ID stage
		if_id_npc : out dlx_word_us     -- Pipelined next PC counter, to ID stage: PC+4
	);
end dlx_pipe_if;

-- ===================================================================
architecture behavior of dlx_pipe_if is
	signal pc : dlx_word_us := (others => '0'); -- present Program Counter

begin

	-- kombinatorischer Anteil
	-- =======================
	ic_addr <= std_logic_vector(pc);    -- Adressierung des Instr.-Memories


	-- sequentieller Anteil
	-- ====================
	if_seq : process(clk)
		variable npc : dlx_word_us; -- next Program Counter
	begin
		if clk'event and clk = '1' then
			if rst = '1' then
				pc       <= X"00_00_00_00";
				if_id_ir <= X"00_00_00_3f";
			else
				-- Instruction Cache Miss: IF and ID-Stage must be stalled
				-- because of Branch-Instructions, the rest of the pipeline
				-- may continue to resolve data dependencies.
				-- ic_ready = '0' => stall = '1'
				if dc_wait = '0' and stall = '0' then
					if_id_ir <= ic_data;
					
					if id_cond = '0' then
						npc := pc + 4;
					elsif id_cond = '1' then
						npc := id_npc;
					end if;
					
					if_id_npc <= npc;
					pc        <= npc;
				end if;
			end if;
		end if;
	end process if_seq;
end behavior;
