-- ===================================================================
-- (C)opyright 2001, 2002
-- 
-- Lehrstuhl Entwurf Mikroelektronischer Systeme
-- Prof. Wehn
-- Universitaet Kaiserslautern
-- 
-- ===================================================================
-- 
-- Autoren:  Frank Gilbert
--           Christian Neeb
--           Timo Vogt
-- 
-- ===================================================================
-- 
-- Projekt: Mikroelektronisches Praktikum
--          SS 2002
-- 
-- ===================================================================
-- 
-- Modul:
-- DLX Pipeline: Instruction Decode Stufe.
-- 
-- ===================================================================
--
-- $Author: gilbert $
-- $Date: 2002/06/18 09:36:09 $
-- $Revision: 2.1 $
-- 
-- ===================================================================

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.dlx_global.all;
use work.dlx_opcode_package.all;
-- synopsys translate_off
use work.dlx_debug.all;
-- synopsys translate_on

-- ===================================================================
entity dlx_pipe_id is
	port(
		clk                : in  std_logic; -- System clock
		rst                : in  std_logic; -- System reset, active HIGH, synchronous
		stall              : in  std_logic; -- stop IF and ID stage because of a not resolvable data dependency
		dc_wait            : in  std_logic; -- wait for memory to finish read operation --> stalled pipeline

		-- NPC and IR from IF-Stage
		if_id_npc          : in  dlx_word_us; -- (PC + 4) provided by IF stage. Used by ID stage to calculate the destination address in case of a program jump or accepted program branch 
		if_id_ir           : in  dlx_word; -- Current Instruction Register in ID stage provided by the IF stage

		-- Data from Register-File
		id_a               : in  dlx_word; -- Source operand A from the register with address id_ir_rs1
		id_b               : in  dlx_word; -- Source operand B from the register with address id_ir_rs2

		-- Instr. Data to/from Forwarding-Ctrl
		id_opcode_class    : out Opcode_class; -- Identification of the syntactical class of the current instruction. Used to resolve conflicts in the pipeline issued by data dependency
		id_ir_rs1          : out Regadr; -- Address of Rs1 to Register file
		id_ir_rs2          : out Regadr; -- Address of Rs2 to Register file
		id_a_fwd_sel       : in  Forward_select; -- Selects the forwarded operand A's value to calculate the jump/branch destination address. Set by the pipeline control unit
		ex_mem_alu_out     : in  dlx_word; -- Result of the ALU calculation of the previous clock cycle in EX/MEM Pipeline Register

		-- Ctrl-Signals for the DLX-Pipeline, IF Stage
		id_cond            : out std_logic; -- Control signal decides whether (PC+4) or id_npc is to be fetched by IF stage
		id_npc             : out dlx_word_us; -- The address of the next instruction in case of an accepted program jump
		id_illegal_instr   : out std_logic; -- Indicate the environment a not defined instruction code
		id_halt            : out std_logic; --Indicate that the program execution was stopped by an Exception

		-- Ctrl-Signals for the DLX-Pipeline, EX/MEM/WB-Stage
		id_ex_a            : out dlx_word := (others => '0'); -- Pipeline Register of the source operand A from the register with the address id_ir_rs1
		id_ex_b            : out dlx_word := (others => '0'); -- Pipeline Register of the source operand B from the register with the address id_ir_rs2
		id_ex_imm          : out Imm17    := (others => '0'); -- Pipeline Register of the immediate operand, sign extended
		id_ex_alu_func     : out Alu_func; -- Pipeline Register of the signal bundle for triggering the ALU in the EX stage
		id_ex_alu_opb_sel  : out std_logic; -- Pipeline Register of the control signal to select operand B in the EX stage
		id_ex_dm_en        : out std_logic; -- Pipeline Register for the enable signal of the data memory
		id_ex_dm_wen       : out std_logic; -- Pipeline Register for the write enable of the data memory
		id_ex_dm_width     : out Mem_width; -- Pipeline Register which determine whether the data memory is accessed in Byte, Halfword or Word format
		id_ex_us_sel       : out std_logic; -- Pipeline Register which distinguishes between unsigned and signed arithmetic in the EX stage
		id_ex_data_sel     : out std_logic; -- Pipeline Register for source selection for the register Rd to store data during the MEM stage
		id_ex_reg_rd       : out RegAdr; -- Pipeline Register for the Rd address for the data to be written during the MEM stage
		id_ex_reg_wen      : out std_logic; -- Pipeline Register for the write enable signal of the register memory
		id_ex_opcode_class : out Opcode_class; -- Pipeline Register of id_opcode_class
		id_ex_ir_rs1       : out RegAdr; -- Pipeline Register of id_ir_rs1
		id_ex_ir_rs2       : out RegAdr -- Pipeline Register of id_ir_rs2
	);

end dlx_pipe_id;

-- ===================================================================
architecture behavior of dlx_pipe_id is
	signal iar                 : dlx_word_us; -- Interrupt Address Register
	signal id_a_fwd            : dlx_word; -- Operand A in forwarding
	signal id_opcode_class_int : Opcode_class; -- Opcode class of current instruction to its Pipeline Register

	signal if_id_ir_opcode   : Opcode;  -- Opcode field of current instruction
	signal if_id_ir_rs1      : Regadr;  -- Rs1 Register's address field of current Instruction
	signal if_id_ir_rs2      : Regadr;  -- Rs2 Register's address field of current Instruction
	signal if_id_ir_rd_rtype : Regadr;  -- Rd Register's address field of R-type Instruction
	signal if_id_ir_rd_itype : Regadr;  -- Rd Register's address field of I-type Instruction
	signal if_id_ir_spfunc   : Spfunc;  -- Special Function field of R-R ALU Instruction
	signal if_id_ir_fpfunc   : Fpfunc;  -- Special Function field of Floating Point Instruction
	signal if_id_ir_imm16    : std_logic_vector(0 to 15); -- 16-bit immediate operand field of current instruction
	signal if_id_ir_imm26    : std_logic_vector(0 to 25); -- 26-but immediate operand field of current instruction

begin

	-- splitting of instruction word 
	if_id_ir_opcode   <= if_id_ir(0 to 5);
	if_id_ir_rs1      <= if_id_ir(6 to 10);
	if_id_ir_rs2      <= if_id_ir(11 to 15);
	if_id_ir_rd_rtype <= if_id_ir(16 to 20);
	if_id_ir_rd_itype <= if_id_ir(11 to 15);
	if_id_ir_spfunc   <= if_id_ir(26 to 31);
	if_id_ir_fpfunc   <= if_id_ir(27 to 31);
	if_id_ir_imm16    <= if_id_ir(16 to 31);
	if_id_ir_imm26    <= if_id_ir(6 to 31);

	id_ir_rs1       <= if_id_ir_rs1;
	id_ir_rs2       <= if_id_ir_rs2;
	id_opcode_class <= id_opcode_class_int;

	-- =================================================================
	-- multiplexing operand a (forwarding)
	-- =================================================================
	id_fwd_a_mux : with id_a_fwd_sel select id_a_fwd <=
		ex_mem_alu_out when FWDSEL_EX_MEM_ALU_OUT,
		id_a when others;

	-- =================================================================
	-- combinatorial logic
	-- =================================================================
	id_comb : process(if_id_ir_imm16, if_id_ir_imm26, if_id_ir_opcode, if_id_npc, id_a_fwd, if_id_ir_spfunc)
		variable imm16 : unsigned(0 to 31); -- Sign-extended 32-bit immediate operand from 16-bit immediate operand
		variable imm26 : unsigned(0 to 31); -- Sign-extended 32-bit immediate operand from 26-bit immediate operand

	begin
		id_cond          <= '0';
		id_illegal_instr <= '0';
		id_halt          <= '0';
		id_npc           <= (others => '-');

		-- Address Calculation: Signed extended 32-Unsigned-Reg. + 16/26-Signed-Immediate
		imm16(0 to 15)  := (0 to 15 => if_id_ir_imm16(0));
		imm16(16 to 31) := unsigned(if_id_ir_imm16);
		imm26(0 to 5)   := (0 to 5 => if_id_ir_imm26(0));
		imm26(6 to 31)  := unsigned(if_id_ir_imm26);

		case if_id_ir_opcode is
			-- ====================================================
			-- Your code for combinatorial decoder logic goes here
			-- ====================================================
			when op_special =>
				case if_id_ir_spfunc is
					when sp_add | sp_addu | sp_and | sp_or | sp_seq | sp_sge | sp_sgt | sp_sle | 
						 sp_sll | sp_slt | sp_sne | sp_sra | sp_srl | sp_sub | sp_subu | sp_xor =>
						id_opcode_class_int <= RR_ALU;
					when sp_nop =>
						id_opcode_class_int <= NOFORW;
					when others => null;
				end case;
			when op_addi =>
				id_opcode_class_int <= IM_ALU;
			when op_addui =>
				id_opcode_class_int <= IM_ALU;
			when op_andi =>
				id_opcode_class_int <= IM_ALU;
			when op_beqz =>
				id_opcode_class_int <= BRANCH;
				if id_a_fwd = X"00000000" then
					id_cond <= '1';
					id_npc  <= if_id_npc + imm16;
				else
					id_npc <= if_id_npc;
				end if;
			when op_bnez =>
				id_opcode_class_int <= BRANCH;
				if id_a_fwd /= X"00000000" then
					id_cond <= '1';
					id_npc  <= if_id_npc + imm16;
				else
					id_npc <= if_id_npc;
				end if;
			when op_j =>
				id_opcode_class_int <= NOFORW;
				id_cond             <= '1';
				id_npc              <= if_id_npc + imm26;
			when op_jalr =>
				id_opcode_class_int <= BRANCH;
				id_cond             <= '1';
				id_npc              <= unsigned(id_a_fwd);
			when op_jal =>
				id_opcode_class_int <= NOFORW;
				id_cond             <= '1';
				id_npc              <= if_id_npc + imm26;
			when op_jr =>
				id_opcode_class_int <= BRANCH;
				id_cond             <= '1';
				id_npc              <= unsigned(id_a_fwd);
			when op_lb | op_lbu | op_lh | op_lhi | op_lhu | op_lw =>
				id_opcode_class_int <= LOAD;
			when op_ori =>
				id_opcode_class_int <= IM_ALU;
			when op_sb =>
				id_opcode_class_int <= STORE;
			when op_seqi =>
				id_opcode_class_int <= IM_ALU;
			when op_sgei =>
				id_opcode_class_int <= IM_ALU;
			when op_sgti =>
				id_opcode_class_int <= IM_ALU;
			when op_sh =>
				id_opcode_class_int <= STORE;
			when op_slei =>
				id_opcode_class_int <= IM_ALU;
			when op_slli =>
				id_opcode_class_int <= IM_ALU;
			when op_slti =>
				id_opcode_class_int <= IM_ALU;
			when op_snei =>
				id_opcode_class_int <= IM_ALU;
			when op_srai =>
				id_opcode_class_int <= IM_ALU;
			when op_srli =>
				id_opcode_class_int <= IM_ALU;
			when op_subi =>
				id_opcode_class_int <= IM_ALU;
			when op_subui =>
				id_opcode_class_int <= IM_ALU;
			when op_sw =>
				id_opcode_class_int <= STORE;
			when op_trap =>
				id_halt             <= '1';
				id_opcode_class_int <= NOFORW;
			when op_xori =>
				id_opcode_class_int <= IM_ALU;
			when others =>
				id_opcode_class_int <= NOFORW;
				id_illegal_instr    <= '1';
		end case;
	end process id_comb;

	-- =================================================================
	-- sequential logic (clocked process)
	-- =================================================================
	id_seq : process(clk)
	begin
		if clk'event and clk = '1' then
			if rst = '1' then
				iar                <= X"00_00_00_00";
				id_ex_dm_wen       <= '0';
				id_ex_dm_en        <= '0';
				id_ex_reg_wen      <= '0';
				id_ex_a            <= X"00_00_00_00";
				id_ex_b            <= X"00_00_00_00";
				id_ex_imm          <= '0' & X"00_00";
				id_ex_opcode_class <= NOFORW;

			else

				-- synopsys translate_off
				debug_disassemble(if_id_ir);
				-- synopsys translate_on

				if dc_wait = '0' then
					-- NOP Behavior as Default
					id_ex_a           <= id_a;
					id_ex_b           <= id_b;
					id_ex_imm         <= if_id_ir_imm16(0) & if_id_ir_imm16;
					id_ex_alu_opb_sel <= SEL_ID_EX_B;
					id_ex_us_sel      <= SEL_SIGNED;
					id_ex_data_sel    <= SEL_ALU_OUT;
					id_ex_alu_func    <= alu_add;
					id_ex_dm_width    <= MEM_WIDTH_WORD;
					id_ex_dm_wen      <= '0';
					id_ex_dm_en       <= '0';
					id_ex_reg_rd      <= REG_0;
					id_ex_reg_wen     <= '0';
					id_ex_ir_rs1      <= if_id_ir_rs1;
					id_ex_ir_rs2      <= if_id_ir_rs2;

					if stall = '1' then
						id_ex_opcode_class <= NOFORW;
					else
						id_ex_opcode_class <= id_opcode_class_int;

						-- different behavior from NOP
						case if_id_ir_opcode is
							-- ====================================================
							-- Your code for sequential decoder logic goes here
							-- ====================================================   								
							when op_special =>
								id_ex_reg_rd  <= if_id_ir_rd_rtype;
								id_ex_reg_wen <= '1';
								case if_id_ir_spfunc is
									when sp_add =>
										id_ex_alu_func <= alu_add;
									when sp_addu =>
										id_ex_alu_func <= alu_add;
										id_ex_us_sel   <= SEL_UNSIGNED;
									when sp_and =>
										id_ex_alu_func <= alu_and;
									when sp_or =>
										id_ex_alu_func <= alu_or;
									when sp_seq =>
										id_ex_alu_func <= alu_seq;
									when sp_sge =>
										id_ex_alu_func <= alu_sge;
									when sp_sgt =>
										id_ex_alu_func <= alu_sgt;
									when sp_sle =>
										id_ex_alu_func <= alu_sle;
									when sp_sll =>
										id_ex_alu_func <= alu_sll;
									when sp_slt =>
										id_ex_alu_func <= alu_slt;
									when sp_sne =>
										id_ex_alu_func <= alu_sne;
									when sp_sra =>
										id_ex_alu_func <= alu_sra;
									when sp_srl =>
										id_ex_alu_func <= alu_srl;
									when sp_sub =>
										id_ex_alu_func <= alu_sub;
									when sp_subu =>
										id_ex_alu_func <= alu_sub;
										id_ex_us_sel   <= SEL_UNSIGNED;
									when sp_xor =>
										id_ex_alu_func <= alu_xor;
									when others => null;
								end case;
							when op_addi =>
								id_ex_alu_opb_sel <= SEL_ID_EX_IMM;
								id_ex_reg_rd      <= if_id_ir_rd_itype;
								id_ex_reg_wen     <= '1';
							when op_andi =>
								id_ex_alu_func    <= alu_and;
								id_ex_alu_opb_sel <= SEL_ID_EX_IMM;
								id_ex_reg_rd      <= if_id_ir_rd_itype;
								id_ex_reg_wen     <= '1';
							when op_ori =>
								id_ex_alu_func    <= alu_or;
								id_ex_alu_opb_sel <= SEL_ID_EX_IMM;
								id_ex_reg_rd      <= if_id_ir_rd_itype;
								id_ex_reg_wen     <= '1';
							when op_seqi =>
								id_ex_alu_func    <= alu_seq;
								id_ex_alu_opb_sel <= SEL_ID_EX_IMM;
								id_ex_reg_rd      <= if_id_ir_rd_itype;
								id_ex_reg_wen     <= '1';
							when op_sgei =>
								id_ex_alu_func    <= alu_sge;
								id_ex_alu_opb_sel <= SEL_ID_EX_IMM;
								id_ex_reg_rd      <= if_id_ir_rd_itype;
								id_ex_reg_wen     <= '1';
							when op_sgti =>
								id_ex_alu_func    <= alu_sgt;
								id_ex_alu_opb_sel <= SEL_ID_EX_IMM;
								id_ex_reg_rd      <= if_id_ir_rd_itype;
								id_ex_reg_wen     <= '1';
							when op_slei =>
								id_ex_alu_func    <= alu_sle;
								id_ex_alu_opb_sel <= SEL_ID_EX_IMM;
								id_ex_reg_rd      <= if_id_ir_rd_itype;
								id_ex_reg_wen     <= '1';
							when op_slli =>
								id_ex_alu_func    <= alu_sll;
								id_ex_alu_opb_sel <= SEL_ID_EX_IMM;
								id_ex_reg_rd      <= if_id_ir_rd_itype;
								id_ex_reg_wen     <= '1';
							when op_slti =>
								id_ex_alu_func    <= alu_slt;
								id_ex_alu_opb_sel <= SEL_ID_EX_IMM;
								id_ex_reg_rd      <= if_id_ir_rd_itype;
								id_ex_reg_wen     <= '1';
							when op_snei =>
								id_ex_alu_func    <= alu_sne;
								id_ex_alu_opb_sel <= SEL_ID_EX_IMM;
								id_ex_reg_rd      <= if_id_ir_rd_itype;
								id_ex_reg_wen     <= '1';
							when op_srai =>
								id_ex_alu_func    <= alu_sra;
								id_ex_alu_opb_sel <= SEL_ID_EX_IMM;
								id_ex_reg_rd      <= if_id_ir_rd_itype;
								id_ex_reg_wen     <= '1';
							when op_srli =>
								id_ex_alu_func    <= alu_srl;
								id_ex_alu_opb_sel <= SEL_ID_EX_IMM;
								id_ex_reg_rd      <= if_id_ir_rd_itype;
								id_ex_reg_wen     <= '1';
							when op_subi =>
								id_ex_alu_func    <= alu_sub;
								id_ex_alu_opb_sel <= SEL_ID_EX_IMM;
								id_ex_reg_rd      <= if_id_ir_rd_itype;
								id_ex_reg_wen     <= '1';
							when op_xori =>
								id_ex_alu_func    <= alu_xor;
								id_ex_alu_opb_sel <= SEL_ID_EX_IMM;
								id_ex_reg_rd      <= if_id_ir_rd_itype;
								id_ex_reg_wen     <= '1';
							when op_addui =>
								id_ex_alu_func    <= alu_add;
								id_ex_imm         <= '0' & if_id_ir_imm16;
								id_ex_alu_opb_sel <= SEL_ID_EX_IMM;
								id_ex_us_sel      <= SEL_UNSIGNED;
								id_ex_reg_rd      <= if_id_ir_rd_itype;
								id_ex_reg_wen     <= '1';
							when op_subui =>
								id_ex_alu_func    <= alu_sub;
								id_ex_imm         <= '0' & if_id_ir_imm16;
								id_ex_alu_opb_sel <= SEL_ID_EX_IMM;
								id_ex_us_sel      <= SEL_UNSIGNED;
								id_ex_reg_rd      <= if_id_ir_rd_itype;
								id_ex_reg_wen     <= '1';
							when op_beqz =>
							when op_bnez =>
							when op_j    =>
							when op_jalr =>
								id_ex_reg_rd  <= REG_31;
								id_ex_a       <= std_logic_vector(if_id_npc);
								id_ex_b       <= X"00_00_00_04";
								id_ex_us_sel  <= SEL_UNSIGNED;
								id_ex_reg_wen <= '1';
							when op_jal =>
								id_ex_reg_rd  <= REG_31;
								id_ex_a       <= std_logic_vector(if_id_npc);
								id_ex_b       <= X"00_00_00_04";
								id_ex_us_sel  <= SEL_UNSIGNED;
								id_ex_reg_wen <= '1';
							when op_jr =>
							when op_lb =>
								id_ex_alu_opb_sel <= SEL_ID_EX_IMM;
								id_ex_data_sel    <= SEL_DM_OUT;
								id_ex_dm_width    <= MEM_WIDTH_BYTE;
								id_ex_dm_en       <= '1';
								id_ex_reg_rd      <= if_id_ir_rd_itype;
								id_ex_reg_wen     <= '1';
							when op_lbu =>
								id_ex_imm         <= '0' & if_id_ir_imm16;
								id_ex_alu_opb_sel <= SEL_ID_EX_IMM;
								id_ex_us_sel      <= SEL_UNSIGNED;
								id_ex_data_sel    <= SEL_DM_OUT;
								id_ex_dm_width    <= MEM_WIDTH_BYTE;
								id_ex_dm_en       <= '1';
								id_ex_reg_rd      <= if_id_ir_rd_itype;
								id_ex_reg_wen     <= '1';
							when op_lh =>
								id_ex_alu_opb_sel <= SEL_ID_EX_IMM;
								id_ex_data_sel    <= SEL_DM_OUT;
								id_ex_dm_width    <= MEM_WIDTH_HALFWORD;
								id_ex_dm_en       <= '1';
								id_ex_reg_rd      <= if_id_ir_rd_itype;
								id_ex_reg_wen     <= '1';
							when op_lhi =>
								id_ex_alu_opb_sel <= SEL_ID_EX_IMM;
								id_ex_data_sel    <= SEL_DM_OUT;
								id_ex_dm_width    <= MEM_WIDTH_HALFWORD;
								id_ex_dm_en       <= '1';
								id_ex_reg_rd      <= if_id_ir_rd_itype;
								id_ex_reg_wen     <= '1';
							when op_lhu =>
								id_ex_imm         <= '0' & if_id_ir_imm16;
								id_ex_alu_opb_sel <= SEL_ID_EX_IMM;
								id_ex_us_sel      <= SEL_UNSIGNED;
								id_ex_data_sel    <= SEL_DM_OUT;
								id_ex_dm_width    <= MEM_WIDTH_HALFWORD;
								id_ex_dm_en       <= '1';
								id_ex_reg_rd      <= if_id_ir_rd_itype;
								id_ex_reg_wen     <= '1';
							when op_lw =>
								id_ex_alu_opb_sel <= SEL_ID_EX_IMM;
								id_ex_data_sel    <= SEL_DM_OUT;
								id_ex_dm_width    <= MEM_WIDTH_WORD;
								id_ex_dm_en       <= '1';
								id_ex_reg_rd      <= if_id_ir_rd_itype;
								id_ex_reg_wen     <= '1';
							when op_sb =>
								id_ex_alu_opb_sel <= SEL_ID_EX_IMM;
								id_ex_data_sel    <= SEL_ALU_OUT;
								id_ex_dm_width    <= MEM_WIDTH_BYTE;
								id_ex_dm_wen      <= '1';
								id_ex_dm_en       <= '1';
								id_ex_reg_rd      <= if_id_ir_rd_itype;
							when op_sh =>
								id_ex_alu_opb_sel <= SEL_ID_EX_IMM;
								id_ex_data_sel    <= SEL_ALU_OUT;
								id_ex_dm_width    <= MEM_WIDTH_HALFWORD;
								id_ex_dm_wen      <= '1';
								id_ex_dm_en       <= '1';
								id_ex_reg_rd      <= if_id_ir_rd_itype;
							when op_sw =>
								id_ex_alu_opb_sel <= SEL_ID_EX_IMM;
								id_ex_data_sel    <= SEL_ALU_OUT;
								id_ex_dm_width    <= MEM_WIDTH_WORD;
								id_ex_dm_wen      <= '1';
								id_ex_dm_en       <= '1';
								id_ex_reg_rd      <= if_id_ir_rd_itype;
							when op_trap =>
								iar <= if_id_npc;
							when others => null;
						-- Tue nichts. Illegal-Instruktion wird kombinatorisch
						-- generiert !!!
						end case;
					end if;             -- stall
				end if;                 -- wait
			end if;                     -- rst
		end if;                         -- clk
	end process id_seq;

end behavior;
