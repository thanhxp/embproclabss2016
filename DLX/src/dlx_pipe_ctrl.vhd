-- ===================================================================
-- (C)opyright 2001, 2002
-- 
-- Lehrstuhl Entwurf Mikroelektronischer Systeme
-- Prof. Wehn
-- Universitaet Kaiserslautern
-- 
-- ===================================================================
-- 
-- Autoren:  Frank Gilbert
--           Christian Neeb
--           Timo Vogt
-- 
-- ===================================================================
-- 
-- Projekt: Mikroelektronisches Praktikum
--          SS 2002
-- 
-- ===================================================================
-- 
-- Modul:
-- DLX Pipeline Controller. Erzeugt Pipeline-Stall bei nicht 
-- aufloesbaren Datenabhaengigkeiten und steuert die "forwarding"
-- Logik.
-- 
-- ===================================================================
--
-- $Author: gilbert $
-- $Date: 2002/06/18 09:36:09 $
-- $Revision: 2.1 $
-- 
-- ===================================================================

library ieee;
use ieee.std_logic_1164.all;
use work.dlx_global.all;

-- ===================================================================
entity dlx_pipe_ctrl is
	port(
		id_ir_rs1           : in  RegAdr; -- Source register 1 of current instruction in ID stage
		id_ir_rs2           : in  RegAdr; -- Source register 2 of current instruction in ID stage
		id_opcode_class     : in  Opcode_class; -- Instruction type of current instruction in ID stage
		id_ex_opcode_class  : in  Opcode_class; -- Instruction type of current instruction in EX stage, in ID/EX Pipeline Register --> Previous instruction in EX stage
		id_ex_ir_rs1        : in  RegAdr; -- Source register 1 of current instruction in EX stage, in ID/EX Pipeline Register --> Previous instruction in EX stage
		id_ex_ir_rs2        : in  RegAdr; -- Source register 2 of current instruction in EX stage, in ID/EX Pipeline Register -->
		id_ex_reg_rd        : in  RegAdr; -- Destination register of current instruction in EX stage
		ex_mem_opcode_class : in  Opcode_class; -- Instruction type of current instruction in MEM stage
		ex_mem_reg_rd       : in  RegAdr; -- Destination register of current instruction in MEM stage
		mem_wb_opcode_class : in  Opcode_class; -- Instruction type of current instruction in WB stage
		mem_wb_reg_rd       : in  RegAdr; -- Instruction type of current instruction in WB stage
		ic_ready            : in  std_logic; -- Instruction cache ready
		dc_ready            : in  std_logic; -- Data cache ready
		id_a_fwd_sel        : out Forward_select; -- Selects the forwarded operand A's value to calculate the jump/branch destination address
		ex_alu_opa_sel      : out Forward_select; -- Select the forwarded operand A's value for ALU in EX stage
		ex_alu_opb_sel      : out Forward_select; -- Select the forwarded operand B's value for ALU in EX stage
		ex_dm_data_sel      : out Forward_select; -- Select the forwarded data value to store in Data memory in EX stage
		stall               : out std_logic; -- Halt ID, EX stage to resolve data dependencies, cannot resolve by forwarding
		dc_wait             : out std_logic -- Wait for memory to finish read operation --> stalled pipeline
	);

end dlx_pipe_ctrl;

-- ===================================================================
architecture behavior of dlx_pipe_ctrl is
begin
	dc_wait <= not dc_ready;

	-- =================================================================
	-- Pipeline-Stall control to resolve data-dependencies
	-- =================================================================
	stall_ctrl : process(id_opcode_class, id_ex_opcode_class, ex_mem_opcode_class, id_ir_rs1, id_ir_rs2, id_ex_reg_rd, ex_mem_reg_rd, ic_ready)
	begin
		-- default stall
		stall <= '0';
		if ic_ready = '0' then
			-- Instruction Cache Miss: IF and ID-Stage must be stalled
			-- because of Branch-Instructions, the rest of the pipeline
			-- may continue to resolve data dependencies.
			-- ic_ready = '0' => stall = '1'
			stall <= '1';
		else
			case id_opcode_class is
				when BRANCH | MOVEI2S => -- b.c BRAND or MOVEI2S executed in ID stage
					case id_ex_opcode_class is
						when LOAD | RR_ALU | IM_ALU | MOVES2I =>
							if id_ir_rs1 = id_ex_reg_rd then
								stall <= '1';
							end if;
						when others => null;
					end case;
					case ex_mem_opcode_class is
						when LOAD | MOVES2I =>
							if id_ir_rs1 = ex_mem_reg_rd then
								stall <= '1';
							end if;
						when others => null;
					end case;
				-- =================================================
				-- Your code goes here (LOAD, STORE, RR_ALU, IM_ALU)
				-- =================================================
				when LOAD | STORE | IM_ALU =>
					if (id_ex_opcode_class = LOAD) and id_ir_rs1 = id_ex_reg_rd then
						stall <= '1';
					end if;

				when RR_ALU =>
					if (id_ex_opcode_class = LOAD) and (id_ir_rs1 = id_ex_reg_rd or id_ir_rs2 = id_ex_reg_rd) then
						stall <= '1';
					end if;

				when others =>
			-- Do Nothing
			end case;
		end if;                         -- if ic_ready = '1' then
	end process stall_ctrl;

	-- =================================================================
	-- Forwarding Control
	-- =================================================================
	pipe_ctrl : process(id_opcode_class, id_ex_opcode_class, id_ex_ir_rs1, id_ex_ir_rs2, id_ex_reg_rd, id_ir_rs1, ex_mem_opcode_class, ex_mem_reg_rd, mem_wb_opcode_class, mem_wb_reg_rd)
	begin
		-- ==================================
		-- Default behaviour: no forwarding
		-- ==================================    
		id_a_fwd_sel   <= FWDSEL_NOFORW;
		ex_alu_opa_sel <= FWDSEL_NOFORW;
		ex_alu_opb_sel <= FWDSEL_NOFORW;
		ex_dm_data_sel <= FWDSEL_NOFORW;

		-- ======================================
		-- Forward EX/MEM -> ID (BRANCH/MOVEI2S)
		-- ======================================
		case id_opcode_class is
			when BRANCH | MOVEI2S =>
				if (ex_mem_opcode_class = RR_ALU or ex_mem_opcode_class = IM_ALU
				) and id_ir_rs1 = ex_mem_reg_rd then
					id_a_fwd_sel <= FWDSEL_EX_MEM_ALU_OUT;
				end if;
			when others =>
		-- Do Nothing
		end case;

		-- ===============================================
		-- Forward EX/MEM, MEM/WB -> EX (LOAD/STORE/RR_ALU/IM_ALU)
		-- ===============================================
		case id_ex_opcode_class is
			-- ===============================
			-- Your code goes here
			-- ===============================
			when RR_ALU =>
				case ex_mem_opcode_class is
					when RR_ALU | IM_ALU | MOVES2I =>
						if (id_ex_ir_rs1 = ex_mem_reg_rd) then
							ex_alu_opa_sel <= FWDSEL_EX_MEM_ALU_OUT;
						end if;
						if (id_ex_ir_rs2 = ex_mem_reg_rd) then
							ex_alu_opb_sel <= FWDSEL_EX_MEM_ALU_OUT;
						end if;
					when others => null;
				end case;
				case mem_wb_opcode_class is
					when RR_ALU | IM_ALU | MOVES2I | LOAD =>
						if (id_ex_ir_rs1 = mem_wb_reg_rd) then
							ex_alu_opa_sel <= FWDSEL_MEM_WB_DATA;
						end if;
						if (id_ex_ir_rs2 = mem_wb_reg_rd) then
							ex_alu_opb_sel <= FWDSEL_MEM_WB_DATA;
						end if;
					when others => null;
				end case;

			when IM_ALU | LOAD =>
				case ex_mem_opcode_class is
					when RR_ALU | IM_ALU | MOVES2I =>
						if (id_ex_ir_rs1 = ex_mem_reg_rd) then
							ex_alu_opa_sel <= FWDSEL_EX_MEM_ALU_OUT;
						end if;
					when others => null;
				end case;
				case mem_wb_opcode_class is
					when RR_ALU | IM_ALU | MOVES2I | LOAD =>
						if (id_ex_ir_rs1 = mem_wb_reg_rd) then
							ex_alu_opa_sel <= FWDSEL_MEM_WB_DATA;
						end if;
					when others => null;
				end case;

			when STORE =>
				case ex_mem_opcode_class is
					when RR_ALU | IM_ALU | MOVES2I =>
						if (id_ex_ir_rs1 = ex_mem_reg_rd) then
							ex_alu_opa_sel <= FWDSEL_EX_MEM_ALU_OUT;
						end if;
						if (id_ex_reg_rd = ex_mem_reg_rd) then
							ex_dm_data_sel <= FWDSEL_EX_MEM_ALU_OUT;
						end if;
					when others => null;
				end case;
				case mem_wb_opcode_class is
					when RR_ALU | IM_ALU | MOVES2I | LOAD =>
						if (id_ex_ir_rs1 = mem_wb_reg_rd) then
							ex_alu_opa_sel <= FWDSEL_MEM_WB_DATA;
						end if;
						if (id_ex_reg_rd = mem_wb_reg_rd) then
							ex_dm_data_sel <= FWDSEL_MEM_WB_DATA;
						end if;
					when others => null;
				end case;
			when others =>
		-- Do Nothing
		end case;

		-- =============================
		-- Forward MEM/WB -> MEM (STORE)
		-- =============================
		case ex_mem_opcode_class is
			when STORE =>
				case mem_wb_opcode_class is
					when LOAD =>
						if ex_mem_reg_rd = mem_wb_reg_rd then
							ex_dm_data_sel <= FWDSEL_MEM_WB_DATA;
						end if;
					when others =>
				end case;
			when others =>
		-- Do Nothing
		end case;

	end process pipe_ctrl;

end behavior;
 
