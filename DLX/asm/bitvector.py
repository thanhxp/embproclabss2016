from math import ceil, log

# ==============================================================================
class bit(object):

    def __init__(self, value=0):
        self._value = int(bool(value))

    # --------------------------------- representation -------------------------
    def __str__(self)    : return '%d' % self._value
    def __repr__(self)   : return '(bit: %d)' % self._value
    def __copy__(self)   : return self.__class__(self._value)
    def __invert__(self) : return self.__class__( ~self._value )
    def __nonzero__(self): return bool(self._value)
    def __int__(self)    : return int(self._value)
    def __long__(self)   : return long(self._value)
    def __float__(self)  : return float(self._value)
    def __oct__(self)    : return oct(self._value)
    def __hex__(self)    : return hex(self._value)

# ==============================================================================
class bitvector(object):

    # --------------------------------------------------------------------------
    def __init__(self, value=0, width=None):

        if hasattr(value, 'value'): value = value.value # for e.g. signals...

        if isinstance(value, bitvector):
            if width is None: width = value._width
            value = value._value

        elif isinstance(value, (int, long)):

            if width is None:
                width = value
                value = 0

            if not width:
                raise TypeError, 'Bitvector must have non-zero width.'
        
        elif isinstance(value, str):

            if width is None: width = len(value)

            try: value  = long(value, 2)
            except ValueError:
                raise ValueError('Not a valid bit-string value: "%s"' % value)

        elif isinstance(value, bit):
            width = 1
            value = int(value)
            
        else:
            raise TypeError, \
                 'bitvector should be initialized by bitvector, int or a string, '+\
                 'got: %s' % repr(value)
            
        self._width = width
        self._setValue(value)

    # --------------------------------------------------------------------------
    width   = property (lambda self: self._width)

    # --------------------------------------------------------------------------
    def rotleft(self, amount=1):
        amount = amount % self._width
        if not amount: return self
        return self.__class__( (self << amount) +
                               self[:self.width-amount],
                               self._width
                             )

    # --------------------------------------------------------------------------
    def rotright(self, amount=1):
        amount = amount % self._width
        if not amount: return self        
        return self.__class__( (self >> amount) +
                               (int(self[amount:]._value) << (self._width-amount)),
                               self._width
                             )

    # --------------------------------------------------------------------------
    def iterup(self):
        '''Iterate from lsb (leeft) up to msb (left)'''        
        return iter([self[i] for i in range(0, self._width)])

    # --------------------------------------------------------------------------
    def iterdown(self):
        '''Iterate from msb (left) down to lsb (right)'''
        return iter([self[i] for i in range(self._width-1, -1, -1)])
    
    # --------------------------------------------------------------------------
    def _setValue(self, value):

        self._value = value & ((1L << self._width)-1) # restrict val to width bits
        
    # --------------------------------------------------------------------------
    def __hash__(self):
        return hash(self._value)
    
    def __copy__(self):
        return self.__class__( self._value, self._width )
    
    def __deepcopy__(self, visit):
        return self.__class__( self._value, self._width )

    def __iter__(self):
        return self.iterdown()

    # -------------------------------- length ----------------------------------
    def __len__(self): return self._width

    # ------------------------ indexing and slicing methods --------------------
    def __getitem__(self, index):

        if isinstance (index, slice):
            # [i:j]
            if index.step: raise ValueError, 'Step not supported!'

            i = index.start or self._width
            j = index.stop  or 0

            if i < 0: i = self._width + i
            elif not (0 < i <= self._width): 
                raise ValueError, '%s[%d:%s] first slice-index out of range (width=%s)!' % \
                                  (self.__class__.__name__, i, j, self._width)

            if not (0 <= j <= self._width-1): 
                raise ValueError, '%s[%s:%s] second slice-index out of range (width=%d)!' % \
                                  (self.__class__.__name__, i, j, self._width)

            if i <= j:
                raise ValueError, '%s[i:j] requires i > j (i=%s, j=%s)!' % \
                                  (self.__class__.__name__,i,j)
            return self.__class__( (self._value>>j) & ((1L<<(i-j))-1), i-j )
        
        else:
            # [i]
            if index < 0: index = self._width + index
            if not (0 <= index < self._width):
                raise ValueError, '%s[%d] index out of range (width=%d)!' % \
                                  (self.__class__.__name__, index, self._width)

            return int((self._value >> index) & 1)
        
    # ------------------------------------------------------
    def __setitem__(self, index, value):

        if isinstance(index, slice):
            # [i:j]
            if index.step: raise ValueError, 'Step not supported!'

            i = index.start or self._width
            j = index.stop  or 0
                      
            # val conversion
            limit  = (1L << (i-j))
            length = None

            if isinstance(value, str):
                if (i-j) != len(value):
                    raise ValueError, "Value does not match the slice length!" + \
                                      "(i=%d, j=%d, v='%s')" % (i,j,value) 
                value = long(value,2)
                
            elif isinstance (value, bitvector):
                if (i-j) != len(value):
                    raise ValueError, "Value does not match the slice length!" + \
                                      "(i=%d, j=%d, v='%s')" % (i,j,value) 
                value = value._value

            else:
                value = int(value)

            if i == self._width:
                if j:
                    # [:j] = ...
                    q = self._value % (1L << j)
                    self._setValue((value<<j) + q)
                else:
                    # [:] = ...
                    self._setValue(value)

            if i > j:
                # [i:j] = ...
                mask = (limit-1)<<j # i-j ones shifted left by j: 0011100(i=5,j=2)
                #print 'mask:', unsigned(mask,8), 'value:', unsigned(value << j, 8), value,
                #print unsigned(mask & (value<<j),8),
                #print unsigned(self._value | (mask & (value<<j)),8)
                self._value &= ~mask
                self._value |= mask & (value << j)
                #self._setValue( self._value | ( mask & (value << j) ))

            else:
                raise ValueError, 'bitvector[i:j] requires i > j (i=%s, j=%s)' % (i,j)

        else:
            # [i] = ...
            if index < 0: index = self._width + index
            if int(value) == 0:
                self._setValue( self._value & ~(1L << index) )
            elif int(value) == 1:
                self._setValue( self._value | (1L << index) )
            else:
                raise ValueError, 'Setting bitvector[%d] requires 0 or 1, got "%s"!' % \
                                  (index, value)
           
    # ------------------------ bool/logical operations -------------------------
    def __nonzero__(self):
        return bool(self._value)

    def __eq__(self, other):
        
        if type(other) is str:
            return (self._value == bitvector(other)._value) and \
                   (self._width == len(other))

        elif isinstance(other, bitvector):
            return (self._value == other._value) and \
                   (self._width == other._width)
        return False

    def __ne__(self, other):
        return not self.__eq__(other)

    # ---------------------------- bit operations ------------------------------
    def __invert__(self):
        return self.__class__( (~self._value) & ((1L << self._width)-1), self._width)

    def __and__(self, other):
        return self.__class__( self._value &
                               bitvector(other, self._width)._value, self._width )

    def __rand__(self, other):
        return self.__class__( self._value &
                               bitvector(other, self._width)._value, self._width )

    def __or__(self, other):
        return self.__class__( self._value |
                               bitvector(other, self._width)._value, self._width )

    def __ror__(self, other):
        return self.__class__( self._value |
                               bitvector(other, self._width)._value, self._width )
        
    def __xor__(self, other):
        return self.__class__( self._value ^
                               bitvector(other, self._width)._value, self._width )
        
    def __rxor__(self, other):
        return self.__class__( self._value ^
                               bitvector(other, self._width)._value, self._width )
    

    # -------------------------- bit shift operations --------------------------
    def __lshift__(self, other): 
        return self.__class__(self._value << other, self._width)
    
    def __rshift__(self, other):
        if isinstance(other, bitvector): other = other._value
        return self.__class__(self._value >> other, self._width)

    # --------------------------- representation -------------------------------
    def __str__(self):
        return bin(self)

    def __repr__(self):
        #return "(%s<%d>: '%s'=%d)" % \
        #       (self.__class__.__name__, self._width, bin(self), self._value)
        return "bv[%d]'%s'" % (self._width, bin(self))
    


# ==============================================================================
class unsigned(bitvector):
    ''' Return a unsigned bitvector with specified width.

        value - unsigned value of the bitvector
        width - bit width
    '''
    # --------------------------------------------------------------------------
    def __init__(self, value=0, width=None):

        if hasattr(value, 'value'): value = value.value # for e.g. signals...

        if isinstance(value, bitvector):
            if width is None: width = value._width
            value = value._value

        elif isinstance(value, (int, long)):

            if width is None:
                width = value
                value = 0

            if not width:
                raise TypeError, 'Bitvector must have non-zero width.'

        
        elif isinstance(value, str):

            if width is None: width = len(value)

            try: value  = long(value, 2)
            except ValueError:
                raise ValueError('Not a valid bit-string value: "%s"' % value)
            
        else:
            raise TypeError, \
                 'bitvector should be initialized by bitvector, int or a string, '+\
                 'got: %s' % repr(value)
            
        self._min = 0
        self._max = (1L << width)-1

        self._width = width
        self._setValue(value)

    # --------------------------------------------------------------------------
    min = property (lambda self: self._min)
    max = property (lambda self: self._max)

    # --------------------------------------------------------------------------
    def _setValue(self, value):
        self._value = value & ((1L << self._width)-1) # restrict val to width bits


    # -------------------------------- bit operations --------------------------
    def __eq__(self, other):
        return self._value == other

    def __rlshift__(self, other):
        return other << self._value

    def __rrshift__(self, other):
        return other >> self._value

    # --------------------------- integer-like methods -------------------------
    def __pos__(self)              : return self
    def __abs__(self)              : return self._value
    def __int__(self)              : return int(self._value)
    def __long__(self)             : return long(self._value)
    def __float__(self)            : return float(self._value)
    def __oct__(self)              : return oct(self._value)
    def __hex__(self)              : return hex(self._value)
    def __cmp__(self, other)       : return cmp(self._value, other)
    def __add__(self, other)       : return self._value + other
    def __radd__(self, other)      : return other + self._value
    def __sub__(self, other)       : return self._value - other
    def __rsub__(self, other)      : return other - self._value
    def __mul__(self, other)       : return self._value * other
    def __rmul__(self, other)      : return other * self._value
    def __div__(self, other)       : return self._value / other
    def __rdiv__(self, other)      : return other / self._value
    def __truediv__(self, other)   : return operator.truediv(self._value / other)
    def __rtruediv__(self, other)  : return operator.truediv(other, self._value)
    def __floordiv__(self, other)  : return self._value / other
    def __rfloordiv__(self, other) : return other / self._value
    def __mod__(self, other)       : return self._value % other
    def __rmod__(self, other)      : return other % self._value,
    def __pow__(self, other)       : return self._value ** other
    def __rpow__(self, other)      : return other ** self._value

    def __repr__(self):
        return "u[%d]'%s'" % (self._width, bin(self))
            

# ==============================================================================
class signed(bitvector):
    ''' Return a signed bitvector with specified width.

        value - signed value of the bitvector
        width - bit width
    '''

    def __init__(self, value=0, width=None):

        if hasattr(value, 'value'): value = value.value # for e.g. signals...

        if isinstance(value, bitvector):
            if width is None: width = value._width
            value = value._value

        elif isinstance(value, (int, long)):

            if width is None:
                width = value
                value = 0

            if not width:
                raise TypeError, 'Bitvector must have non-zero width.'
                
        elif isinstance(value, str):

            if width is None: width = len(value)

            try: value  = long(value,2) - (1l<<width)
            except ValueError:
                raise ValueError('Not a valid bit-string value: "%s"' % value)
            
        else:
            raise TypeError, \
                 'Bitvector should be initialized by bitvector, int or a string, '+\
                 'got: %s' % repr(value)
            
        self._min = -(1L << (width-1))
        self._max =  (1L << (width-1))-1

        self._width = width
        self._setValue(value)

    # --------------------------------------------------------------------------
    def _setValue(self, value):

        self._value = (value & ((1L << self._width)-1))
        if self._value > self._max:
            self._value -= 1L << self._width

    # -------------------------------- bit operations --------------------------
    def __eq__(self, other):
        return self._value == other

    # ----------------------------- integer-like methods -----------------------
    def __pos__(self)              : return self
    def __neg__(self)              : return -self._value    
    def __abs__(self)              : return abs(self._value)
    def __int__(self)              : return int(self._value)
    def __long__(self)             : return long(self._value)
    def __float__(self)            : return float(self._value)
    def __oct__(self)              : return oct(self._value)
    def __hex__(self)              : return hex(self._value)
    def __cmp__(self, other)       : return cmp(self._value, other)
    def __add__(self, other)       : return self._value + other
    def __radd__(self, other)      : return other + self._value
    def __sub__(self, other)       : return self._value - other
    def __rsub__(self, other)      : return other - self._value
    def __mul__(self, other)       : return self._value * other
    def __rmul__(self, other)      : return other * self._value
    def __div__(self, other)       : return self._value / other
    def __rdiv__(self, other)      : return other / self._value
    def __truediv__(self, other)   : return operator.truediv(self._value / other)
    def __rtruediv__(self, other)  : return operator.truediv(other, self._value)
    def __floordiv__(self, other)  : return self._value / other
    def __rfloordiv__(self, other) : return other / self._value
    def __mod__(self, other)       : return self._value % other
    def __rmod__(self, other)      : return other % self._value,
    def __pow__(self, other)       : return self._value ** other
    def __rpow__(self, other)      : return other ** self._value

    def __repr__(self):
        return "s[%d]'%s'" % (self._width, bin(self))
    

    # --------------------------------------------------------------------------
    min = property (lambda self: self._min)
    max = property (lambda self: self._max)
        

# ==============================================================================
def bin(num, width=0):
    """
    Return a binary string representation.

        num     -- number to convert
        width   -- specifies the desired string (sign bit padding)
    """
    if isinstance (num, bitvector):
        width = num.width
        num = long(num._value)

    if num == 0 : s = '0'
    elif abs(num) == 1: s = '1'
    else:
        s = ''
        p, q = divmod(num, 2)
        s = str(q) + s
        while not (abs(p) == 1):
            p, q = divmod(p, 2)
            s = str(q) + s
        s = '1' + s

    if width:
        pad = '0'
        if num < 0: pad = '1'
        return (width - len(s)) * pad + s
    return s

# ------------------------------------------------------------------------------
def concat(*args):
    
    width = 0
    first = True
    for arg in args:

        if hasattr(arg, '_width'):
        #if isinstance(arg, (bitvector, Signal)):
            w = arg._width
            v = arg._value
        elif arg in (0,1,True,False):
            w = 1
            v = arg
        elif isinstance(arg, str):
            w = len(arg)
            v = long(arg, 2)
        else:
            raise TypeError("Inappropriate argument to concat: %s (%s)" \
                            % (arg, type(arg)))
        if not w:
            raise TypeError, "concat: arg has no specified width!"

        width += w
        if first:
            value = v
            first = False
        else:
            value = (value << w) + v
    return bitvector(value, width)

# ==============================================================================
if __name__ == '__main__':

    # --- tests ---
    x = bitvector('10011')
    y = bitvector('00111')

    u = unsigned(5,5)
    s = signed(5,5)
    
    a = signed(8)
    print repr(a) 
    assert a == 0; assert len(a) == 8
    
    a = signed('101')
    print repr(a)
    assert a == -3; assert a.width == 3

    a = unsigned('101')
    print repr(a)
    assert a == 5; assert a.width == 3
    
    # cast a to 7 bit
    a = signed(-3,5)
    b = signed(a, 7) 
    print repr((a,b))
    assert a==b

    # arithmetic...
    c = unsigned(5,8) + unsigned(2,8)
    print repr(c)
    assert c == 7

    c = unsigned(5,8) + 2
    print repr(c)
    assert c == 7    

    c = 5 + unsigned(2,8)
    print repr(c)
    assert c == 7    

    c = signed(5 + unsigned(2,8), 8)
    print repr(c)
    assert c == 7    

    # concat...
    c = signed(concat('11', '00', signed(3,3) ))
    print repr(c)
    assert c == -29
    assert c.width == 7

    a = signed(0,5)
    print repr(a)
    assert a+1 == 1

    print '\ntest get slice...'
    x = unsigned('1001'); print repr(x); assert x == 9
    y = x[0];             print repr(y); assert y == 1
    y = x[1];             print repr(y); assert y == 0
    y = x[:];             print repr(y); assert y == 9
    y = x[2:];            print repr(y); assert y == 1
    y = x[:2];            print repr(y); assert y == 2

    print '\ntest slice assignment...'
    x = unsigned('1111'); print repr(x); assert x == 15
    x[1]   = 0;           print repr(x); assert x == 13
    x[2]   = 0;           print repr(x); assert x == 9
    x[-1]  = 0;           print repr(x); assert x == 1
    x[-1]  = '1';         print repr(x); assert x == 9
    x[3:1] = '11';        print repr(x); assert x == 15
    x[3:1] = 2;           print repr(x); assert x == 13
    x[3:1] = 4;           print repr(x); assert x == 9
    x[3:1] = 1;           print repr(x); assert x == 11
    x[3:1] = '00';        print repr(x); assert x == 9
    x[:]   = '1111';      print repr(x); assert x == 15
    x[:]   = '0000';      print repr(x); assert x == 0

    b = bitvector('11001')
    
