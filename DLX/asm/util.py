from inspect           import stack, getargspec
from sets              import Set
from time              import time
#from numarray          import Float32
#from numarray.nd_image import gaussian_filter1d
from os.path           import exists
from random            import expovariate
from math              import ceil
from types             import LambdaType, FunctionType

import inspect, shelve, new, sys, gzip, os, copy_reg


# ------------------------------------------------------------------------------
# ignore function objects for pickling in 'toFile': pickled as 'None'
def ignoreUnpickler() : return None
def ignoreReduce(func): return ignoreUnpickler, ()
copy_reg.pickle(LambdaType, ignoreReduce)

# ------------------------------------------------------------------------------
plotFormats = '-bo', '-rx', '-gs', '-m^', '-c<'
plotColors  = 'b', 'g', 'r', 'c', 'm', 'y', 'k'

# ------------------------------------------------------------------------------
def binom(n, k):

    x = 1.0
    for i in xrange(1,k+1):
        x *= (n+1-i) / float(i)
    return int(x)

# ------------------------------------------------------------------------------
class Record(object):

    def __init__(self, *recs, **kw):
        
        for r in recs:
            self.__dict__.update(vars(r))
        
        self.__dict__.update(kw)

    def __getitem__(self, key):
        return getattr(self, key)

    def __setitem__(self, key, val):
        setattr(self, key, val)

    def __delitem__(self, key):
        try:
            self.__dict__.__delitem__(key)
        except: pass

    def __str__(self):
        return formatObj(self)

    __repr__ = __str__

        
# ------------------------------------------------------------------------------
def argv():
    d = dict(partition(sys.argv[1:],2))
    for k,v in d.iteritems():
        d[k] = eval(v)
    return d
    
# ------------------------------------------------------------------------------
class sequence(object): pass

# ------------------------------------------------------------------------------
def allinstance(seq, types):

    for x in seq:
        if not isinstance(x, types): return False
    return True

# ------------------------------------------------------------------------------
def all(seq):
    for x in seq:
        if not x: return False
    return True

# ------------------------------------------------------------------------------
def any(seq):
    for x in seq:
        if x: return True
    return False

# ------------------------------------------------------------------------------
def uniquify(col):
    return type(col)(set(col))

# ------------------------------------------------------------------------------
def flatten(*args):
    l = []

    if args and args[0] is None:
        return l
        
    for arg in args:
        if issequence(arg):
            for item in arg:
                l.extend(flatten(item))
        else:
            l.append(arg)
    return l

# ------------------------------------------------------------------------------
def issequence(seq):

    'Returns true if seq is a sequence type like list, tuple, set.'
    return isinstance(seq, (sequence, list, tuple, set))
    #try:
    #    iter(seq)
    #    return True
    #except TypeError: return False

# ------------------------------------------------------------------------------
def toTuple(obj):

    'Return a tuple if obj not already is of type tuple.'
    
    if isinstance(obj, tuple): return obj
    if issequence(obj)        : return tuple(obj)
    return (obj, )

# ------------------------------------------------------------------------------
def toList(obj):

    'Return a list if obj not already is of type list.'
    
    if isinstance(obj, list): return obj
    if issequence(obj)    : return [x for x in obj]
    return [obj]

# ------------------------------------------------------------------------------
def toSet(obj):

    'Return a set if obj not already is of type set.'
    
    if isinstance(obj, Set): return obj
    if issequence(obj)   : return ImmutableSet(obj)
    return ImmutableSet([obj])

# ------------------------------------------------------------------------------
def partition(seq, length, pad=None, seqType=list):

    '''Return a partitioned sequence (list of tuples).

    Last tuple might contain less elements than length which is filled
    by pad if given.

        length: length of each partition
        pad   : fill last partition if given
    '''
    #assert issequence(seq), 'Expected a sequence, got "%s".' % seq
    assert length > 0, 'Length must be > 0, got "%s".' % length
    
    it     = iter(seq)
    remain = len(seq)
    result = []
    while remain > 0:
        nv      = min(remain, length)
        remain -= nv
        result.append(seqType(it.next() for i in xrange(nv)))

    if pad != None:
        pad = pad,
        while len(result[-1]) < length:
            result[-1] += seqType(pad)
    return result

# ------------------------------------------------------------------------------
def printObject(obj, *attributes):

    'Prints an object with unrolled dict/list attributes.'
    
    if not attributes: return ''
    s = ''
    l = max([len(a) for a in attributes])
    for attrName in attributes:

        try:
            attr = getattr(obj, attrName)
        except AttributeError:
            attr = getattr(obj.__class__, attrName)
            
        if isinstance(attr, dict):

            if not len(attr): continue
            s += attrName + ':\n'

            for k,v in attr.iteritems():
                if issequence(v):
                    s += '  %s:\n' % k
                    for x in v:
                        s += '    %s\n' % repr(x)
                    s += '\n'   
                else:
                    s += ('  %-10s: %s\n' % (k,repr(v)))
            s += '\n'

        elif issequence(attr):
            if not len(attr): continue
            s += attrName + ':\n'
            for a in attr: s += ('  %s\n' % repr(a))
            s += '\n'
            
        elif isinstance(attr, float):
            s += '%s: %.3f\n' % (attrName, attr)

        else:
            fstr = '%-' + str(l) + 's: %s\n'
            s += fstr % (attrName, repr(attr))
    
    return s

# ------------------------------------------------------------------------------
class Timer(object):

    def __init__(self, autoReset=True):

        self.autoReset = autoReset
        self.init()

    def init(self):
        self.startTime = time()

    def time(self):

        v = time()-self.startTime
        if self.autoReset:
            self.init()
        return v

    def __str__(self):
        return 'time = %.3f' % self.time()

# ------------------------------------------------------------------------------
def filterGauss(seq, sigma=None):

    if sigma is None:
        sigma = len(seq) / 300.0

    if not isinstance(seq, tuple): seq = tuple(seq)
    length = int(round(sigma*10))
    seq = (seq[0],)*length + seq + (seq[-1],)*length
    return gaussian_filter1d(seq, sigma, output=Float32)[length:-length]

# ------------------------------------------------------------------------------
def toFile(obj, filename='data.dat', compress=True):

    from cPickle import dump
    
    if compress: tmpname = filename + '.tmp'
    else       : tmpname = filename
    if exists(filename): os.remove(filename)
    
    dump(obj, open(tmpname,'w'))

    if compress:
        # compress file...
        z = gzip.open(filename, 'w', 5)
        f = open(tmpname)
        z.write(f.read())
        f.close()
        z.close()
        os.remove(tmpname)
    

# --------------------------------------------------------------------------
def fromFile(filename='data.dat'):

    from cPickle import load

    if not exists(filename):
        raise IOError, 'File "%s" does not exists.' % filename

    # try to uncompress file...
    try:
        tmpname = filename + '.tmp'
        z = gzip.open(filename)
        f = open(tmpname, 'w')
        f.write(z.read())
        z.close()
        f.close()
    except IOError:
        z.close()
        f.close()
        os.remove(tmpname)
        tmpname = filename

    try:
        x = load(open(tmpname))
        os.remove(tmpname)
        return x
    except:
        os.remove(tmpname)
        raise

# --------------------------------------------------------------------------
def rgColor(val, lower, upper):

    # lower: green, upper: red
    m = (upper-lower) / 2.0 + lower

    if m-lower == 0: q = 0xff
    else           : q = float(0xff) / (m-lower)

    if val < lower  : val = lower
    elif val > upper: val = upper
    
    if val < m:
        s = '%x' % ((val-lower)*q)
        if len(s) == 1: s = '0'+s
        return '#' + s + 'ff00'
    elif val == m: return '#ffff00'
    else:
        s = '%x' % (0xff - (val-m)*q)
        if len(s) == 1: s = '0'+s
        return '#ff' + s + '00'
    
# --------------------------------------------------------------------------
def choiceExp(seq, mean=0.50):
    '''
    Choose element from sequence with exponantially distributed
    probalility. Left elements have higher probability than right elements.
    '''
    l = len(seq)
    i = int(expovariate(1.0/(mean*l)))
    while i >= l:
        i = int(expovariate(1.0/(mean*l)))
    return seq[i]

# --------------------------------------------------------------------------
def printDict(d, filename='', vwidth=60):
    '''
    Print the local variables and its values.
    '''
    s = ''
    if d:
        l = max([len(str(k)) for k in d.keys()]) + 1
        f = '%-' + str(l) + 's: %s' 
        for k in sorted(d):
            x = repr(d[k]).replace('\n', '').strip()
            if not filename and (len(x) > (vwidth-3)): x = x[:vwidth] + '...'
            s += f % (k,x) + '\n'
    if filename:
        f = open(filename, 'w')
        print >> f, s
        f.close()
    print s
pdi = printDict

# --------------------------------------------------------------------------
def pli( alist ):
    for x in alist: print x

# --------------------------------------------------------------------------
def printLocals(filename='', vwidth=60):
    '''
    Print the local variables and its values.
    '''
    try:
        d = stack()[1][0].f_locals
    except:
        d = {}
    printDict(d, filename, vwidth)

# --------------------------------------------------------------------------
def printObj(obj, filename='', vwidth=60):
    printDict(obj.__dict__, filename)

# ------------------------------------------------------------------------------
def funcargs(func, args=[], kwargs={}):

    s = getargspec(func)
    l = []
    names  = s[0]
    values = s[3]
    result = True

    if len(args) > len(names):
        # too much positional arguments...
        func(*args, **kwargs)
    
    if values is None: values = []
    for i, n in enumerate(names):
        j = i - (len(names)-len(values))
        if j >= 0 :

            # keyword arguments...
            if i < len(args):
                if n in kwargs:
                    # multiple values...
                    l.append( (n, ValueError, values[j]) )
                    result = False
                else:
                    # set positional argument...
                    l.append( (n, args[i], values[j]) )

            elif n in kwargs:
                # set keyword argument...
                l.append( (n, kwargs[n], values[j]) )

            else:
                # set default value
                l.append( (n, values[j], values[j]) )
                
        else:
            # positional arguments...
            if i < len(args):
                l.append( (n, args[i]) )
            else:
                # not enough positional arguments...
                l.append( (n, ValueError) )
                result = False
    return result, l

# ------------------------------------------------------------------------------
def upself(obj):
    ''' Update a objects dict with locals defined in a method. '''
    
    obj.__dict__.update(stack()[1][0].f_locals)

# ------------------------------------------------------------------------------
def wrapMethod(m, before, after=None):

    '''
    Wraps a instance method with a dynamically created method that calls
    the before function, the original method and then the after function.

        before - function accepting the self and the same arguments as the
                 instance method
                 
        after  - function with self as the only argument (default:None)
    '''
    assert callable(m) and callable(before)
    assert (after is None) or callable(after)
    
    obj  = m.im_self
    name = m.__name__    
    obj.__dict__['_wrap_%s' % m.__name__] = m
    

    if after:
        def _wrapped(self, *args, **kargs):
            before(self, *args, **kargs)
            getattr(self, '_wrap_%s' % name)(*args, **kargs)
            after(self)
        _wrapped.__name__ = name

        obj.__dict__[m.__name__] = \
            new.instancemethod(_wrapped, obj, obj.__class__)
        
    else:
        def _wrapped(self, *args, **kargs):
            before(self, *args, **kargs)
            getattr(self, '_wrap_%s' % name)(*args, **kargs)
        _wrapped.__name__ = name

        obj.__dict__[m.__name__] = \
            new.instancemethod(_wrapped, obj, obj.__class__)

# ------------------------------------------------------------------------------
def unwrapMethod(m):

    name = m.__name__
    obj  = m.im_self
    obj.__dict__[name] = getattr(obj, '_wrap_%s' % name)
    del obj.__dict__['_wrap_%s' % name]
    
# ------------------------------------------------------------------------------
def indent(code, amount=2):

    if not code: return ''
    istr = ' '*amount
    return ''.join([istr + line for line in code.splitlines(True)])

# ------------------------------------------------------------------------------
def formatObj(obj, private=False, width=80, attributes=()):

    s = ''
    #keys = [k for k in dir(obj) if (private or not k.startswith('_'))]
    keys = []
    for k in dir(obj):
        if private or not k.startswith('_'):
            if (not attributes) or (k in attributes):
                keys.append(k)
                
    if not keys: return ''
    l = max([len(k) for k in keys]) + 1

    f = '%-' + str(l) + 's: %s' 
    for k in sorted(keys):
        v = getattr(obj, k)
        x = repr(v).replace('\n', '').strip()
        line = f % (k,x)
        if width and len(line) > width: line = line[:width-2] + '..'
        s += line + '\n'
    return s

# ------------------------------------------------------------------------------
def formatDict(d, width=80, useStr=True):

    s = ''
    if d:
        l = max([len(str(k)) for k in d.keys()]) + 1
        f = '%-' + str(l) + 's: %s' 
        for k in sorted(d):
            if useStr: x = str(d[k]).replace('\n', '').strip()
            else     : x = repr(d[k]).replace('\n', '').strip()
            
            line = f % (k,x)
            if width and len(line) > width: line = line[:width-2] + '..'
            s += line + '\n'
    return s

# ------------------------------------------------------------------------------
def format(x, width=80):
    if isinstance(x, dict): return formatDict(x, width=width)
    else                  : return formatObj (x, width=width)

# ------------------------------------------------------------------------------
def pdir(obj, private=False, width=80, attributes=()):

    ' Pretty print an object. '
    if isinstance(obj, dict):
        print formatDict(obj, width)
    else:    
        print formatObj(obj, private, width, attributes)

# ------------------------------------------------------------------------------
def pseq(l, width=80):
    for p in l:
        print str(p)[:80]

# --------------------------------------------------------------------------
def funcLocals(func):

    '''Returns the locals dict of the func. 

       This names equal the dereferenced func-closure cells and the
       passed arguments which have all the None value.
    '''
    def tracefunc(frame, event, arg):
        consts.update(frame.f_locals)
        raise NotImplementedError
    try:
        consts = {}
        sys.setprofile(tracefunc)
        args = (None,) * func.func_code.co_argcount
        func(*args)
        #func()
    except NotImplementedError: pass
    sys.settrace(None)
    return consts

# ------------------------------------------------------------------------------
def __wrap(v=None):
    def f(): return v
    return f
__f     = __wrap()
__deref = lambda cell: (new.function ( __f.func_code,
                                       __f.func_globals,
                                      '',
                                      __f.func_defaults,
                                      (cell,)
                                     ))
def derefCell(cell):
    'dereferences a cell of a functions closure.'
    return __deref(cell)()

def getCells(func):
    '''return a list of tuples (cell-name, cell-deref-value) of a functions
    closure.
    The cell names are stored in the code objects co_freevars list.
    '''
    l = []
    if func.func_closure is None: return l
    for k,v in zip(func.func_code.co_freevars, func.func_closure):
        l.append( (k, derefCell(v)) )
    return l

def makeCell(obj):
    'return a cell object refering to obj to build function closures.'
    def f():obj
    return f.func_closure[0]

def cloneFunc(func, globals=None, name=None, argdefs=None, closure={}):

    if globals is None: globals = func.func_globals
    if argdefs is None: argdefs = func.func_defaults

    cells     = getCells(func)
    cellNames = set(x[0] for x in cells)    
    #print 'cells:', cells
    #print 'names:', cellNames
    
    newClosure = []

    for k,v in cells:
        if k in closure: newClosure.append( makeCell(closure[k]) )
        else           : newClosure.append( makeCell(v) )

    #for k,v in closure.iteritems():
    #    if k in cellNames: continue
    #    newClosure.append( makeCell(closure[k]))
        
    #print 'newClosure:', tuple(newClosure)            
    return new.function( func.func_code, globals,
                         name, argdefs, tuple(newClosure) )
    
# ------------------------------------------------------------------------------
def gcd(a,b):
    'Greates common divider.'

    while b:
        a,b = b, a%b
    return a

# ------------------------------------------------------------------------------
def lcm(a,b):
    'Least common multiple.'
    
    return (a*b)/gcd(a,b)


# ------------------------------------------------------------------------------
if __name__ == '__main__':

    from pylab   import hist, show
    from numpy   import zeros, array
    from inspect import *
    from pdb     import pm
    import sys

##    x = zeros ((4,4))
##    
##    print x
##
##    for i in xrange(1, 10):
##        print partition( range(10), i)

    #a = array(partition(xrange(50),9, pad=-1, seqType=list))
    #print a

    print lcm(40,25)
    
    
