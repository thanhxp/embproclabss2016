from util      import Record, pdi, pli
from bitvector import signed, unsigned, concat, bin
from opcodes   import optab
from re        import match
from pdb       import pm
from sys       import argv, exit

LOADS     = set( ('lb', 'lbu', 'lh', 'lhu', 'lw') )
STORES    = set( ('sb', 'sh', 'sw') )
IUNSIGNED = set( ('addui', 'sequi', 'sgeui', 'sgtui', 'sleui', 'sltui',
                   'sneui', 'subui') )
BRANCH    = set( ('beqz', 'bnez') )
JUMPR     = set( ('jalr', 'jr', ) )

# ==============================================================================
class Instr(Record): pass
class Data(Record): pass
# report code...

# ------------------------------------------------------------------------------
def writeLabels():

    ls = [(label, addr) for label, addr in labels.iteritems()]
    ls.sort()

    s = ''
    f = open(fname[:-2] + '.labels', 'w')
    for x in ls:
        hexaddr = hex(x[1])[2:]
        hexaddr = '0' * (8-len(hexaddr)) + hexaddr
        f.write('%-20s %-12s %s\n' % (x[0], hexaddr, x[1]))
    f.close()

# ------------------------------------------------------------------------------
def writeCodeReport():
    s = ''
    f = open(fname[:-2] + '.report', 'w')
    for instr in code:
        addr = hex(instr.addr)[2:]
        while len(addr) < 8: addr = '0' + addr

        if isinstance(instr, Instr):
            if instr.label: lstr = instr.label + ' : '
            else          : lstr = ''
            f.write('%-3d %s %s %s %s\n' % \
                 (instr.lineno, addr, instr.hex, lstr, instr.line))
        else:
            f.write('%-3d %s %s\n' % \
                 (instr.lineno, addr, instr.hex))
    f.close()

# ------------------------------------------------------------------------------
def writeDebug():
    f = open(fname[:-2]+'.debug', 'w')
    for instr in code: f.write(str(instr) + '\n')
    f.close()

# ------------------------------------------------------------------------------
def writeCode():
    f = open(fname[:-2] + '.out', 'w')
    for instr in code:
        f.write('%s %s\n' % (instr.addrhex, instr.hex))
    f.close()


# ------------------------------------------------------------------------------
def error(instr, msg):
    print 'Error in line %d: "%s"\n%s' % (instr.lineno, instr.line, msg)
    print '\n', instr
    raise SyntaxError

# ------------------------------------------------------------------------------
def getValue(s):
    if   s in labels       : return labels[s]
    elif s.startswith('0x'): return int(s, 16)
    elif s.isdigit()       : return int(s)
    error(instr, 'invalid value "%s"' % s)

# ------------------------------------------------------------------------------
def getReg(s):
    if s and s[0] == 'r' and s[1:].isdigit() and int(s[1:]) < 32: return int(s[1:])
    error(instr, 'invalid register "%s"' % s)

# ------------------------------------------------------------------------------
def getOffsetReg(s):

    m = match('(\w*)\((\w*)\)', s)
    if m: return getValue(m.groups()[0]), getReg(m.groups()[1])
    error(instr, 'invalid offset/register "%s"' % s)

def printUsage():
    print 'usage: <sourcefile>'

# ==============================================================================
if len(argv) == 1:
    #print 'usage: <sourcefile>'
    # exit()
    fname = 'VA.s'

elif len(argv) == 2:
    fname = argv[1]

else:
    printUsage()
    error('too many arguments')


text  = open(fname).read()
lines = text.split('\n')

code   = []
lineno = 0
addr   = 0
labels = {}
regtab = dict( [('r%d' % i, unsigned(i,5)) for i in xrange(32)] )
optab  = dict( [(op, (unsigned(val[0]), val[1])) for op, val in optab.iteritems()] )

# ==============================================================================
# parse code... (pass 1)
# ==============================================================================
for i, line in enumerate(lines):

    # strip comments and empty lines...
    line = line.split(';')[0].strip()

    if line:

        # find label...
        x = line.split(':')
        if len(x) > 1:
            label = x[0]
            if label in labels:
                error('label "%s" already defined' % label)
            labels[label] = addr
            line = x[1].strip()
            if not line: continue
        #else:
        #    label = ''

        if line.startswith('.'):

            # special asm directive...
            x    = line.split()
            cmd  = x[0]
            args = x[1:]

            if cmd == '.align':
                # .align n
                # Cause the next data/code loaded to be at the next higher address with
                # the lower n bits zeroed (the next closest address greater than or
                # equal to the current address that is a multiple of 2^{n-1}).
                newAddr = 2**getValue(args[0])
                if newAddr <= addr: newAddr *= 2
                addr = newAddr

            elif cmd == '.space':
                # .space size
                # Move the   current storage pointer forward size bytes (to leave some
                # empty space in memory).
                addr += getValue(args[0])

            elif cmd == '.word':
                # .word w1, w2, w3, w4, ...
                # Store the words listed on the line sequentially in memory.
                args = ''.join(args)
                args = args.split(',')
                for w in args:
                    addrhex = hex(addr)[2:]
                    while len(addrhex) < 8: addrhex = '0' + addrhex
                    code.append(Data( lineno=lineno, addr=addr,
                                      addrhex = addrhex,
                                      value=getValue(w)))
                    addr += 4

        else:
            # asm instruction...
            instr       = Instr()
            instr.line  = lines[i].strip()
            instr.label = label
            code.append(instr)
            label = ''

            # find op...
            instr.op = line.split()[0]
            line = ''.join(line.split()[1:]).strip()

            instr.lineno = lineno
            instr.addr   = addr

            # arguments...
            instr.args = [arg.strip() for arg in line.split(',')]
            addr += 4

    lineno += 1

# write intermediate code for debug...
writeDebug()

# ==============================================================================
# resolve symbols... (pass 2)
# ==============================================================================
for instr in code:

    if isinstance(instr, Instr):

        op     = instr.op
        args   = instr.args

        try:
            opcode = optab[op][0]
            optype = optab[op][1]
        except:
            error(instr, 'Unknown instruction "%s"' % op)

        instr.type = optype

        # ------------------------------------------------------
        if optype == 'r':

            if op == 'nop': args = [0,0,0]
            else:
                # asm: rd, rs1, rs2

                if len(args) != 3: error(instr, 'Expected 3 arguments for "%s"!' % op)
                args = [getReg(arg) for arg in args]

            # r-format: 6 opcode (=0), 5 rs1, 5 rs2, 5 rd, 5 unused 6 func
            instr.image = ( unsigned('000000'),
                            unsigned(args[1], 5),
                            unsigned(args[2], 5),
                            unsigned(args[0], 5),
                            unsigned('00000'),
                            opcode
                         )

        # ------------------------------------------------------
        elif optype == 'i':

            #args = [resolve(arg) for arg in instr.args]

            if op in STORES:

                # asm : offset(rs1), rd
                if len(args) != 2:
                    error(instr, 'Expected 2 arguments for "%s"!' % op)

                offset, rs1 = getOffsetReg(args[0])
                args = [rs1, getReg(args[1]), offset]

            elif op in LOADS:

                # asm : rd, offset(rs1)
                if len(args) != 2:
                    error(instr, 'Expected 2 arguments for "%s"!' % op)

                offset, rs1 = getOffsetReg(args[1])
                args = [rs1, getReg(args[0]), offset]

            elif op in BRANCH:

                # asm : rs1, name
                if len(args) != 2:
                    error(instr, 'Expected 2 arguments for "%s"!' % op)

                args = [getReg(args[0]), 0, getValue(args[1]) - instr.addr - 4]

            elif op in JUMPR:

                # asm : rs1
                if len(args) != 1:
                    error(instr, 'Expected 2 arguments for "%s"!' % op)

                args = [getReg(args[0])]

            else:
                if len(args) != 3:
                    error(instr, 'Expected 3 arguments for "%s"!' % op)

                # asm : rd, rs1, imm
                args = [getReg(args[1]), getReg(args[0]), getValue(args[2])]

            # i-format: 6 opcode, 5 rs1, 5 rd, 16 imm
            instr.image =( opcode,
                           unsigned(args[0], 5),
                           unsigned(args[1], 5),
                           unsigned(args[2], 16),
                         )

        # ------------------------------------------------------
        elif optype == 'j':

            if len(args) != 1: error(instr, 'Expected 1 argument for "%s"!' % op)

            # j-format: 6 opcode, 26 name
            if instr.op == 'trap':
                instr.image = ( opcode,
                                unsigned(getValue(instr.args[0]), 26)
                              )

            else:
                instr.image = ( opcode,
                                unsigned(getValue(instr.args[0])-instr.addr-4, 26)
                              )
        else:
            error('invalid op-type')

        # ------------------------------------------------------
        # building instruction word...
        instr.code    = concat(*instr.image)
        instr.hex     = hex(unsigned(instr.code))[2:-1]
        instr.addrhex = hex(instr.addr)[2:]
        while len(instr.hex) < 8    : instr.hex     = '0' + instr.hex
        while len(instr.addrhex) < 8: instr.addrhex = '0' + instr.addrhex

    elif isinstance(instr, Data):
        instr.hex = hex(long(instr.value))[2:-1]
        while len(instr.hex) < 8: instr.hex = '0' + instr.hex

# write output...
writeLabels()
writeCodeReport()
writeDebug()
writeCode()


# compare with perl assembler
##lines = open('Viterbi_20070230C_chris.S.obj').read().splitlines()[1:]
##for i,line in enumerate(lines):
##    lines[i] = line.split(':')[1].upper()
##
##for i in xrange(len(lines)):
##    if lines[i] != code[i].hex:
##        print 'Error:', code[i].lineno, lines[i], code[i].hex
##        print code[i]
##        print




