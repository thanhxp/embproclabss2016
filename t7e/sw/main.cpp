// Copyright (C) 2011 University of Kaiserslautern
// Microelectronic System Design Research Group
//
// Matthias Jung (jungma@eit.uni-kl.de)
//
// This file is part of the Virtual Platform Demos
// de.uni-kl.eit.vp.demo

extern "C"
{
	#include "system.h"
}

extern "C" void handle_interrupt(void)
{
	// Empty
}

#include "image.h"

class Profiler
{
	private:
		unsigned int _Start;
		char *_Name;

	public:
		Profiler(char *name);
		~Profiler();
};

Profiler::Profiler(char *name)
{
  _Name = name;
	_Start = getSystickCount();
}

Profiler::~Profiler()
{
  unsigned int cycles = getSystickCount() - _Start;
	float time = cycles * 10 / 1000; // usec
  printf("Profiler: %s took %d cycles (~ %d usec)\n", _Name, cycles, (int)time);
}

int main(void)
{

	initTerminal();
	// Do a profiling of this block:
	{
		Profiler p("Main");

		// Insert your code here:
		unsigned char window[9];
		int i,j,y;
		unsigned char temp;
		for (int x = 1; x < HEIGHT - 1; x++) {
			for ( y = 1; y < WIDTH - 1; y++){
				window[0] = IMAGE[x][y];
				window[1] = IMAGE[x-1][y];
				window[2] = IMAGE[x+1][y];
				window[3] = IMAGE[x][y-1];
				window[4] = IMAGE[x][y+1];
				window[5] = IMAGE[x-1][y-1];
				window[6] = IMAGE[x+1][y+1];
				window[7] = IMAGE[x+1][y-1];
				window[8] = IMAGE[x-1][y+1];

				for (i = 0; i < 5; i++) {
					for (j = i+1; j < 9; j++) {
						if (window[i]>window[j]) {
							temp = window[i];
							window[i] = window[j];
							window[j] = temp;
						}
					}
				}
				setPixel(y, x, window[4]);
			}
		}
	}

	flushDisplay();
	flushTerminal();

	while (1)
	{

	}
}

